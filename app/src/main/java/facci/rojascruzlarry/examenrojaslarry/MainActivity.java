package facci.rojascruzlarry.examenrojaslarry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import facci.rojascruzlarry.examenrojaslarry.BDSqlite.DBProfesores;

public class MainActivity extends AppCompatActivity {

    ListView lv_profesores;
    ArrayList<Profesores> listProf;
    ArrayList<String>  listaInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv_profesores = findViewById(R.id.lv_profesores);

        consultaSQlite();

    }

    private void consultaSQlite() {

        DBProfesores conn = new DBProfesores(this, "Examen", null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();

        Profesores profesores =null;
        listProf = new ArrayList<Profesores>();

        Cursor cursor = db.rawQuery("Select * from Profesores", null);

        while(cursor.moveToNext()){
            profesores = new Profesores();
            profesores.setId(cursor.getInt(0));
            profesores.setNombre(cursor.getString(1));
            profesores.setApellido(cursor.getString(2));
            profesores.setEdad(cursor.getInt(3));
            profesores.setMateria(cursor.getString(4));

            listProf.add(profesores);
        }

        mostrarLista();

    }

    private void mostrarLista() {
        listaInfo = new ArrayList<String>();

        for (int i=0; i < listProf.size(); i++){
            listaInfo.add( String.valueOf(listProf.get(i).getId())
                    + " - "  +  listProf.get(i).getNombre() + " - " + listProf.get(i).getMateria());

            ArrayAdapter adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, listaInfo);
            lv_profesores.setAdapter(adaptador);

            lv_profesores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    Intent intent = new Intent(getApplicationContext(), DetalleActivity.class);
                    intent.putExtra("id", listProf.get(position).getId());
                    intent.putExtra("nombre", listProf.get(position).getNombre());
                    intent.putExtra("apellido", listProf.get(position).getApellido());
                    intent.putExtra("edad", listProf.get(position).getEdad());
                    intent.putExtra("materia", listProf.get(position).getMateria());
                    startActivity(intent);
                }
            });


        }



    }
}