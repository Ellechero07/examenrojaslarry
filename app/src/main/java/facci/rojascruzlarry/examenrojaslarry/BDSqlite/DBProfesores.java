package facci.rojascruzlarry.examenrojaslarry.BDSqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBProfesores extends SQLiteOpenHelper {

    public DBProfesores(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Profesores(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre text, apellido text, edad int, materia text)");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES ('Edgardo', 'Panchana', 27, 'Programacion Movil')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Viviana', 'Garcia', 25, 'Informatica')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Jorge', 'Moya', 38, 'Investigacion de Operaciones')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Johnny', 'Larrea', 31, 'Sistemas Operativos')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Jimmy', 'Barcia', 34, 'Matematicas Discretas')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Ruben', 'Solorzano', 37, 'Algebra')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Carlos', 'Villa', 24, 'Programacion')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Pedro', 'Panchana', 22, 'Programacion Movil')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Maria', 'Garcia', 23, 'Informatica')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Johan', 'Moya', 31, 'Investigacion de Operaciones')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Larry', 'Larrea', 35, 'Sistemas Operativos')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Edgar', 'Barcia', 32, 'Matematicas Discretas')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Dario', 'Solorzano', 31, 'Algebra')");
        db.execSQL("INSERT INTO Profesores(nombre, apellido, edad, materia) VALUES('Fernando', 'Villa', 25, 'Programacion')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS Profesores");
        onCreate(db);
    }
}
