package facci.rojascruzlarry.examenrojaslarry;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetalleActivity extends AppCompatActivity {

    TextView tv_id, tv_nombre, tv_apellido, tv_edad, tv_materia;
    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        tv_id = findViewById(R.id.tv_id);
        tv_nombre = findViewById(R.id.tv_nombre);
        tv_apellido = findViewById(R.id.tv_apellido);
        tv_edad = findViewById(R.id.tv_edad);
        tv_materia = findViewById(R.id.tv_materia);
        iv = findViewById(R.id.iv);

        int id = getIntent().getExtras().getInt("id");
        String nombre = getIntent().getExtras().getString("nombre");
        String apellido = getIntent().getExtras().getString("apellido");
        String materia = getIntent().getExtras().getString("materia");
        int edad = getIntent().getExtras().getInt("edad");

        tv_id.setText("ID: " + id);
        tv_nombre.setText("Nombre: " + nombre);
        tv_apellido.setText("Apellido: " + apellido);
        tv_edad.setText("Edad: " + edad );
        tv_materia.setText("Materia: " + materia);




    }
}